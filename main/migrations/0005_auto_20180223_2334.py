# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-02-23 20:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0004_auto_20180223_2329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='name',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='item',
            name='category',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='item',
            name='image',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='item',
            name='name',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='item',
            name='rarity',
            field=models.CharField(max_length=255),
        ),
    ]
