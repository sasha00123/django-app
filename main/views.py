import json

from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView, ListView
from rest_framework import status
from steampy.client import SteamClient, GameOptions
from steampy.models import Asset

from authentication.models import Win, SteamUser
# Create your views here.
from main.models import Case, Item, Category
from onlinecsgo.settings import MEDIA_URL
from onlinecsgo.settings import SOCIAL_AUTH_STEAM_API_KEY


class CategoryListView(ListView):
    model = Category
    template_name = "main/index.html"
    context_object_name = 'categories'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["MEDIA_URL"] = MEDIA_URL
        context["wins"] = Win.objects.order_by("-id")[:10]
        return context


class CaseDetailView(DetailView):
    model = Case
    context_object_name = 'case'
    template_name = "main/case.html"

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["roulette_items"] = [context_data["case"].base_items.order_by('?').first() for _ in range(150)]
        return context_data


def open_case(request, pk):
    if request.user.is_anonymous:
        return HttpResponse({'Login required': 'login with your steam account first'},
                            status=status.HTTP_403_FORBIDDEN)
    case = Case.objects.get(id=pk)
    if request.user.balance < case.cost:
        return HttpResponse({'Not enough money': 'this case is too expensive for you'},
                            status=status.HTTP_402_PAYMENT_REQUIRED)
    with transaction.atomic():
        record = case.record_set.select_for_update().first()
        item = record.item
        record.delete()
    Win.objects.create(case=case, item=item, steam_user=request.user)
    request.user.balance -= case.cost
    request.user.save()
    if case.record_set.count() == 0:
        case.active = False
        case.save()
    can_open = case.active and request.user.balance >= case.cost
    disabled_message = ""
    if not request.user.balance >= case.cost:
        disabled_message = "Пополните баланс"
    elif not case.active:
        disabled_message = "Кейс временно недоступен"
    return HttpResponse(json.dumps({
        "pk": item.pk,
        "name": item.base.name,
        "image": item.base.image,
        "rarity": item.base.rarity,
        "category": item.base.category,
        "rarity_class": item.base.rarity_class,
        "market_name": item.market_name,
        "cost": item.cost,
        "balance": request.user.balance,
        "can_open": can_open,
        "disabled_message": disabled_message
    }))


class UserProfile(DetailView):
    model = SteamUser
    context_object_name = "user"
    template_name = "main/profile-v2.html"


def my_profile(request):
    if request.user.is_anonymous:
        return HttpResponse({'Login required': 'login with your steam account first'},
                            status=status.HTTP_403_FORBIDDEN)
    return render(request, "main/profile-v2.html", context={"user": request.user, "is_myself": True})


def send_item(request, item):
    return False  # to disable for testing
    if request.user.is_anonymous:
        return HttpResponse({'Login required': 'login with your steam account first'},
                            status=status.HTTP_403_FORBIDDEN)
    steam_client = SteamClient(SOCIAL_AUTH_STEAM_API_KEY)
    steam_client.login("Eonixpharos", "Onlinecsgo123", "onlinecsgo/steam_guard.txt")
    if not request.user.trade_link:
        return HttpResponse({"Not executed": "Trade link required", },
                            status=status.HTTP_406_NOT_ACCEPTABLE)
    win = Win.objects.filter(steam_user=request.user, item=Item.objects.get(pk=item), state=0).first()
    item = win.item
    asset = Asset(item.steam_id, GameOptions.CS)
    response = steam_client.make_offer_with_url([asset], [], request.user.trade_link,
                                                "Your prize from onlinecsgo.com!")
    if "strError" not in response:
        win.state = 1
        win.save()
    else:
        return HttpResponse(json.dumps(response), status=status.HTTP_406_NOT_ACCEPTABLE)
    return HttpResponse(json.dumps(response))


def sell_item(request, item):
    if request.user.is_anonymous:
        return HttpResponse({'Login required': 'login with your steam account first'},
                            status=status.HTTP_403_FORBIDDEN)
    win = Win.objects.filter(steam_user=request.user, item=Item.objects.get(pk=item), state=0).first()
    if win is None:
        return HttpResponse(json.dumps({
            "result": "ERROR"
        }))
    request.user.balance += win.item.cost
    request.user.save()
    win.state = 2
    win.save()
    win.item.used = False
    win.item.base.quantity += 1
    win.item.base.save()
    win.item.save()
    return HttpResponse(json.dumps({"result": "OK", "balance": request.user.balance}))


def save_link(request):
    if request.user.is_anonymous:
        return HttpResponse({'Login required': 'login with your steam account first'},
                            status=status.HTTP_403_FORBIDDEN)
    request.user.trade_link = request.POST["link"]
    request.user.save()
    return HttpResponse(json.dumps({"result": "saved"}))
