from django.db import models, transaction


class BaseItem(models.Model):
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=500)
    quantity = models.PositiveIntegerField()
    category = models.CharField(max_length=255)
    rarity = models.CharField(max_length=255)
    rarity_class = models.CharField(max_length=255)
    class_id = models.BigIntegerField()

    def __str__(self):
        return f"{self.name} - {self.category} - {self.rarity} - {self.quantity}шт"


class Item(models.Model):
    base = models.ForeignKey(BaseItem, on_delete=models.CASCADE)
    cost = models.FloatField()
    steam_id = models.BigIntegerField(unique=True)
    market_name = models.CharField(max_length=255)
    used = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return f"{self.market_name} - {self.cost}р"


class Category(models.Model):
    name = models.CharField(max_length=255)
    exclusive = models.BooleanField()

    def __str__(self):
        return self.name


# Create your models here.
class Case(models.Model):
    name = models.CharField(max_length=255)
    base_items = models.ManyToManyField(BaseItem, through='Entry', blank=True)
    cost = models.IntegerField(default=0, blank=True)
    image = models.ImageField(upload_to="covers")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="cases")
    active = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.base_items.count()} items - {self.cost}р"

    def delete(self, using=None, keep_parents=False):
        with transaction.atomic():
            for record in self.record_set.select_for_update().all():
                record.item.used = False
                record.item.save()
                record.item.base.quantity += 1
                record.item.base.save()
        return super(Case, self).delete(using, keep_parents)


class Entry(models.Model):
    base_item = models.ForeignKey(BaseItem, on_delete=models.CASCADE)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    num = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.base_item.__str__()


class Record(models.Model):
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
