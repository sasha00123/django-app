from django.urls import path

from main import views

urlpatterns = [
    path('', views.CategoryListView.as_view(), name='index'),
    path('case/<int:pk>', views.CaseDetailView.as_view(), name='open-case'),
    path('open_case/<int:pk>', views.open_case),
    path('profile', views.my_profile, name='my-profile'),
    path('profile/<int:pk>', views.UserProfile.as_view(), name='profile'),
    path('send/<int:item>', views.send_item),
    path('sell/<int:item>', views.sell_item),
    path('link', views.save_link)
]
