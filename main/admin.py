from math import ceil

from django.contrib import admin
from django.db import transaction
from numpy.random import choice

# Register your models here.
from main.models import Case, Item, BaseItem, Category, Record
from onlinecsgo.config import PROFIT


class BaseItemInline(admin.StackedInline):
    model = Case.base_items.through
    fields = ["base_item", "num"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "base_item":
            kwargs["queryset"] = BaseItem.objects.filter(case=None)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ItemInline(admin.StackedInline):
    model = Item
    field = ["cost", "steam_id", "market_name"]


@admin.register(BaseItem)
class BaseItemAdmin(admin.ModelAdmin):
    list_display = ["name", "category", "rarity", "quantity"]
    inlines = [ItemInline]
    search_fields = ["name"]
    list_filter = ["rarity"]


def update_case(modeladmin, request, queryset):
    for case in queryset:
        with transaction.atomic():
            for record in case.record_set.select_for_update().all():
                record.item.used = False
                record.item.base.quantity += 1
                record.item.base.save()
                record.item.save()
                record.delete()
        items = []
        if len(items) == 0:
            case.active = False
            case.save()
            return
        for bi in case.base_items.all():
            items += list(bi.item_set.filter(used=False)[:bi.entry_set.first().num])
            bi.quantity = max(0, bi.quantity - bi.entry_set.first().num)
            bi.save()
        n = len(items)
        asum = sum([item.cost for item in items])
        ssum = sum([item.cost ** 2 for item in items])
        profit = PROFIT
        cost = ceil(asum / n * profit)
        for i in range(n):
            k = ssum / (asum * profit)
            sump = sum([k / item.cost for item in items])
            p = [k / (item.cost * sump) for item in items]
            item = choice(items, p=p)
            asum -= item.cost
            ssum -= item.cost ** 2
            Record.objects.create(case=case, item=item)
            item.used = True
            item.save()
            items.remove(item)
        case.cost = cost
        case.active = True
        case.save()


@admin.register(Case)
class CaseAdmin(admin.ModelAdmin):
    fields = ["name", "image", "category", "active"]
    inlines = [BaseItemInline]
    list_display = ["name", "cost", "category", "active"]
    search_fields = ["name", "category__name"]
    list_filter = ["category", "active"]
    actions = [update_case]

    def save_related(self, request, form, formsets, change):
        form.save_m2m()
        for formset in formsets:
            self.save_formset(request, form, formset, change=change)
        case = form.instance
        items = []
        for bi in case.base_items.all():
            items += list(bi.item_set.filter(used=False)[:bi.entry_set.first().num])
            bi.quantity = max(0, bi.quantity - bi.entry_set.first().num)
            bi.save()
        n = len(items)
        asum = sum([item.cost for item in items])
        ssum = sum([item.cost ** 2 for item in items])
        profit = PROFIT
        cost = ceil(asum / n * profit)
        for i in range(n):
            k = ssum / (asum * profit)
            sump = sum([k / item.cost for item in items])
            p = [k / (item.cost * sump) for item in items]
            item = choice(items, p=p)
            asum -= item.cost
            ssum -= item.cost ** 2
            Record.objects.create(case=case, item=item)
            item.used = True
            item.save()
            items.remove(item)
        case.cost = cost
        case.save()


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ["name", "exclusive"]
    list_display = ["name", "exclusive"]
    search_fields = ["name"]


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    fields = ["base", "cost", "steam_id", "market_name"]
    list_display = ["market_name", "cost", "used"]
    search_fields = ["market_name"]
    list_filter = ["used"]


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    field = ["case", "item"]
    list_display = ["case", "item"]
