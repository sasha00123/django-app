function sendLink() {
    axios.post("/link", $("#sidebar-link-form").serialize())
        .then(function (response) {
            $.snackbar({"content": "Ссылка успешно изменена!"});
        })
        .catch(function (response) {
            $.snackbar({"content": "Ссылка не изменена! Попробуйте позже."});
        });
}