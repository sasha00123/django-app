$.fn.shuffleChildren = function () {
    $.each(this.get(), function (index, el) {
        var $el = $(el);
        var $find = $el.children();

        $find.sort(function () {
            return 0.5 - Math.random();
        });

        $el.empty();
        $find.appendTo($el);
    });
};

function openCase(id) {
    $('.card').first().attr("style", "marginLeft: 0px");
    $('cardList').shuffleChildren();
    axios.get("/open_case/" + id).then(function (response) {
        var prize = response.data;
        $('#card145').attr("class", "card " + prize.rarity_class);
        $('#img145').attr("src", prize.image);
        if (!response.data.can_open) {
            $('#open-button')
                .attr("class", "contracts--start case-disabled")
                .attr("href", "")
                .text(response.data.disabled_message);
        }
        var shift = $('#card145').offset().left - $('#arrow').offset().left + random(0, 100);
        $('.card').first().animate({
            marginLeft: -shift
        }, 10000, "easeOutExpo", function () {
            $('#result_modal').show();
            $('#result_title').html(prize.market_name);
            $('#result_price').text(prize.cost + "₽");
            $('#prize_image').attr("src", prize.image);
            $('#result_circle').attr("class", "m-result--circle " + prize.rarity_class);
            $('#result_type').attr("class", "m-result--type " + prize.rarity_class);
            $('#sendButton').off().on("click", function () {
                axios.get("/send/" + prize.pk).then(function (response) {
                    $('#result_modal').hide();
                    $.snackbar({"content": "Предмет успешно отправлен"});
                }).catch(function (response) {
                    $.snackbar({"content": "Произошла ошибка. Возможно, вы не указали ссылку для обмена или у вас закрыт доступ к обмену"});
                });
                return false;
            });
            $('#sellButton').off().on("click", function () {
                axios.get("/sell/" + prize.pk).then(function (response) {
                    $('#result_modal').hide();
                    $("#balance").text(response.data.balance);
                    $.snackbar({"content": "Баланс успешно пополнен"});
                }).catch(function (response) {
                    $.snackbar({"content": "Произошла ошибка. Возможно, отсутствует интернет подключение"});
                });
                return false;
            });
        });


    });
    return false;
}

function random(min, max) {
    return Math.floor((Math.random() * (max - min)) + min);
}

function sellItem(id, win_id) {
    axios.get("/sell/" + id).then(function (response) {
        $("#stateClass" + win_id).attr("class", "have-item is-yellow");
        $("#stateText" + win_id).html("Продано");
        $("#ss" + win_id).html("");
        $("#balance").text(response.data.balance);
        $("#balance-p").text(response.data.balance);
        $.snackbar({"content": "Баланс успешно пополнен"});
    }).catch(function (response) {
        $.snackbar({"content": "Произошла ошибка. Возможно, отсутствует интернет подключение"});
    });
}

function sendItem(id, win_id) {
    axios.get("/send/" + id).then(function (response) {
        $('#stateClass' + win_id).attr("class", "have-item is-green");
        $('#stateText' + win_id).text("Отправлено");
        $("#ss" + win_id).html("");
        $.snackbar({"content": "Предмет успешно отправлен"});
    }).catch(function (response) {
        $.snackbar({"content": "Произошла ошибка. Возможно, вы не указали ссылку для обмена или у вас закрыт доступ к обмену"});
    });
}