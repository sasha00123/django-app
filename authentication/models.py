from datetime import datetime
from math import ceil

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from main.models import Item, Case


class SteamUserManager(BaseUserManager):
    def _create_user(self, steamid, password, **extra_fields):
        """
        Creates and saves a User with the given steamid and password.
        """
        try:
            # python social auth provides an empty email param, which cannot be used here
            del extra_fields['email']
        except KeyError:
            pass
        if not steamid:
            raise ValueError('The given steamid must be set')
        user = self.model(steamid=steamid, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, steamid, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(steamid, password, **extra_fields)

    def create_superuser(self, steamid, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(steamid, password, **extra_fields)


class SteamUser(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'steamid'
    DEFAULT_AFFILIATE_ID = 1
    steamid = models.CharField(max_length=17, unique=True)
    personaname = models.CharField(max_length=255)
    profileurl = models.CharField(max_length=300)
    avatar = models.CharField(max_length=255)
    avatarmedium = models.CharField(max_length=255)
    avatarfull = models.CharField(max_length=255)
    balance = models.PositiveIntegerField(default=0)
    items_won = models.ManyToManyField(Item, blank=True, through='Win')
    trade_link = models.CharField(max_length=500, blank=True, null=True)
    affiliate = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, default=DEFAULT_AFFILIATE_ID)

    # Add the other fields that can be retrieved from the Web-API if required

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = SteamUserManager()

    def get_short_name(self):
        return self.personaname

    def get_full_name(self):
        return self.personaname

    def __str__(self):
        return self.get_full_name()

    def days_joined(self):
        return (datetime.now(timezone.utc) - self.date_joined).days

    def money_won(self):
        return ceil(sum([item.cost for item in self.items_won.all()]))


class Win(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    steam_user = models.ForeignKey(SteamUser, on_delete=models.CASCADE)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)
    state = models.PositiveSmallIntegerField(default=0)

    def get_current_state_name(self):
        states = ["Производится выбор", "Продано", "Отправлено"]
        return states[self.state]
