# Generated by Django 2.0.2 on 2018-03-09 21:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentication', '0009_steamuser_trade_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='steamuser',
            name='trade_link',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
