import time

from django_cron import CronJobBase, Schedule
from steampy.client import SteamClient, GameOptions

from main.models import Item, BaseItem
from .settings import SOCIAL_AUTH_STEAM_API_KEY

steam_client = SteamClient(SOCIAL_AUTH_STEAM_API_KEY)
steam_client.login("Eonixpharos", "Onlinecsgo123", "onlinecsgo/steam_guard.txt")
rarities = {
    "Ширпотреб": "is-c1",
    "Промышленное качество": "is-c2",
    "Армейское качество": "is-c3",
    "Запрещенное": "is-c4",
    "Засекреченное": "is-c5",
    "Тайное": "is-c6"
}


class UpdateInventory(CronJobBase):
    schedule = Schedule(run_every_mins=60)
    code = 'onlinecsgo.update_inventory'

    def do(self):
        count = 0
        my_inventory = steam_client.get_my_inventory(GameOptions.CS)
        for item in [item for item in my_inventory.values() if item["tags"][1]["category"] == "Weapon"]:
            if count % 20 == 0:
                time.sleep(60)
            count += 1
            data = {
                "name": item["name"].split(" | ")[0],
                "category": item["name"].split(" | ")[1],
                "image": "http://cdn.steamcommunity.com/economy/image/" + item["icon_url"],
                "quantity": 0,
                "rarity": item["type"].split(", ")[-1],
                "rarity_class": rarities[item["type"].split(", ")[-1]],
                "class_id": item["classid"]
            }
            base_item, _ = BaseItem.objects.get_or_create(class_id=item["classid"], defaults=data)
            item_data = {
                "base": base_item,
                "cost": float(
                    steam_client.market.fetch_price(item["market_hash_name"],
                                                    GameOptions.CS)["median_price"][1:-4]) * 56,
                "market_name": item["market_name"]
            }
            item_created, created = Item.objects.get_or_create(steam_id=item["id"], defaults=item_data)
            item_created.cost = item_data["cost"]
            item_created.save()
            if created:
                base_item.quantity += 1
                base_item.save()
