from django.contrib import admin
from payments.models import PaymentLog

# Register your models here.
@admin.register(PaymentLog)
class PaymentLogAdmin(admin.ModelAdmin):
    list_display = ["user", "amount", "created"]