from django.shortcuts import render
from hashlib import md5
from onlinecsgo import config
from authentication.models import SteamUser
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import HttpResponse, get_object_or_404
from payments.models import PaymentLog

@csrf_exempt
@require_POST
def result(request):
    m = md5()
    m.update(':'.join([config.ANY_PAY_APP_ID, request.POST['amount'], request.POST['pay_id'], config.ANY_PAY_SECRET_KEY]).encode("utf-8"))
    signature = m.hexdigest()
    if signature != request.POST['sign']:
        return HttpResponse('INVALID SIGN1ATURE!')
    user_id = int(request.POST['pay_id'])
    user = get_object_or_404(SteamUser, pk=user_id)
    PaymentLog.objects.create(amount=int(request.POST['amount']), user=user)
    user.balance += int(request.POST['amount'])
    user.save()
    user.affiliate.balance += int(request.POST['amount']) * config.AFFILIATE_PERCENT
    user.affiliate.save()
    return HttpResponse('OK')

def success(request):
    return render(request, 'payments/success.html')

def fail(request):
    return render(request, 'payments/error.html')
