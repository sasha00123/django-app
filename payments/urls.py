
from django.urls import path

from payments import views

urlpatterns = [
  path('result', views.result),
  path('success', views.success),
  path('fail', views.fail),
]
