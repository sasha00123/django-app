from django.db import models
from authentication.models import SteamUser
from django.utils import timezone

class PaymentLog(models.Model):
    amount = models.PositiveIntegerField()
    user = models.ForeignKey(SteamUser, on_delete=models.CASCADE, related_name="payments")
    created = models.DateTimeField(default=timezone.now)